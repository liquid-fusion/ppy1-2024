import requests
import psycopg2
from datetime import datetime
import zipfile
from io import BytesIO
import time  

# Parameters for connecting to the PostgreSQL database
db_config = {
    "database": "postgres",
    "user": 'postgres',
    "password": '<DO_NOT_FORGET_TO_SET_UP>',
    "host": "localhost",
    "port": "5433"
}

# Parameters for API request
API_KEY = "<YOUR_API_KEY>"
BASE_URL = "http://api.openweathermap.org/data/2.5/forecast?"
UNITS = "metric"

def get_city_data():
    try:
        # Downloading city data from the GeoNames database
        response = requests.get("http://download.geonames.org/export/dump/cities5000.zip")
        cities = []
        with response, zipfile.ZipFile(BytesIO(response.content)) as z:
            with z.open('cities5000.txt') as f:
                for line in f:
                    cities.append(line.decode('utf-8').split('\t')[1])
        return cities
    except Exception as e:
        print(f"Error loading city data: {e}")
        return None

def get_forecast_data(city_name):
    try:
        response = requests.get(f"{BASE_URL}q={city_name}&units={UNITS}&appid={API_KEY}")
        data = response.json()
        forecasts = []
        for item in data['list']:
            dt_txt = item['dt_txt']
            if "12:00:00" in dt_txt:
                forecasts.append({
                    "city_name": city_name,
                    "date_time": dt_txt,
                    "temp_min": item['main']['temp_min'],
                    "temp_max": item['main']['temp_max'],
                    "humidity": item['main']['humidity'],
                    "cloudiness": item['clouds']['all'],
                    "wind_speed": item['wind']['speed']
                })
        return forecasts
    except Exception as e:
        print(f"Error when receiving data from the API for the city {city_name}: {e}")
        return []

def save_forecast_to_db(forecasts):
    try:
        conn = psycopg2.connect(**db_config)
        cursor = conn.cursor()
        
        for forecast in forecasts:
            cursor.execute(
                """
                INSERT INTO forecast (city_name, date_time, temp_max, temp_min, humidity, cloudiness, wind_speed)
                VALUES (%s, %s, %s, %s, %s, %s, %s)
                """,
                (forecast['city_name'], forecast['date_time'], forecast['temp_min'], forecast['temp_max'], forecast['humidity'], forecast['cloudiness'], forecast['wind_speed'])
            )

        conn.commit()
        cursor.close()
        conn.close()
        print("The weather forecast has been successfully saved to the database.")
    except Exception as e:
        print(f"Error when saving data to the database: {e}")

def main():
    cities = get_city_data()
    if cities:
        while True:
            for city in cities:
                forecasts = get_forecast_data(city)
                if forecasts:
                    save_forecast_to_db(forecasts)
                else:
                    print(f"The weather forecast data for the city could not be obtained {city}.")
            
            time.sleep(600)  # Pause of 10 minutes
    else:
        print("Couldn't get a list of cities. Check your internet connection and the availability of the data source.")

if __name__ == "__main__":
    main()
