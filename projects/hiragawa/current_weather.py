import requests
import psycopg2
from datetime import datetime
import time  
from io import BytesIO
import zipfile
import csv  

# Parameters for connecting to the PostgreSQL database
db_config = {
    "database": "postgres",
    "user": 'postgres',
    "password": '<YOUR_PASSWORD>',
    "host": "localhost",
    "port": "5433"
}

# Parameters for API request
API_KEY = "<YOUR_API_KEY>"
BASE_URL = "http://api.openweathermap.org/data/2.5/weather?"
UNITS = "metric"

def get_city_data():
    try:
        # Downloading city data from the GeoNames database
        response = requests.get("http://download.geonames.org/export/dump/cities5000.zip")
        with zipfile.ZipFile(BytesIO(response.content)) as z:
            with z.open('cities5000.txt') as f:
                cities = [line.decode('utf-8').split('\t')[1] for line in f]
        return cities
    except Exception as e:
        print(f"Error loading city data: {e}")
        return None

def get_weather_data(city_name):
    try:
        response = requests.get(f"{BASE_URL}q={city_name}&units={UNITS}&appid={API_KEY}")
        data = response.json()
        return {
            "city_name": city_name,
            "temperature": data['main']['temp'],
            "humidity": data['main']['humidity'],
            "cloudiness": data['clouds']['all'],
            "wind_speed": data['wind']['speed']
        }
    except Exception as e:
        print(f"Error when receiving data from the API for the city {city_name}: {e}")
        return None

def save_data_to_db(weather_data):
    try:
        conn = psycopg2.connect(**db_config)
        cursor = conn.cursor()
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        
        cursor.execute(
            """
            INSERT INTO current_weather (record_time, city_name, temperature, humidity, cloudiness, wind_speed)
            VALUES (%s, %s, %s, %s, %s, %s)
            """,
            (now, weather_data['city_name'], weather_data['temperature'], weather_data['humidity'], weather_data['cloudiness'], weather_data['wind_speed'])
        )

        conn.commit()
        cursor.close()
        conn.close()
        print(f"The data for the city {weather_data['city_name']} has been successfully saved to the database.")
    except Exception as e:
        print(f"Error when saving data to the database for the city {weather_data['city_name']}: {e}")

def main():
    cities = get_city_data()  # Getting a list of cities
    if cities:
        while True:  
            for city in cities:
                weather_data = get_weather_data(city)
                if weather_data:
                    save_data_to_db(weather_data)
                else:
                    print(f"Couldn't get weather data for the city {city}.")
            
            time.sleep(1000)  # Pause of 10 minutes
    else:
        print("Couldn't get a list of cities. Check your internet connection and the availability of the data source.")

if __name__ == "__main__":
    main()
